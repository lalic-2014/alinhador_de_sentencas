<text lang="es" id="es">
<p><s>Hallan dientes del más antiguo de los orangutanes</s>
</p>
<p><s>Una nueva especie de homínido hallada en Tailandia, con una antigüedad estimada en 12 millones de años, se ha convertido en el pariente más remoto de los actuales orangutanes ( Pongo pygmaeus ).</s><s>Un grupo de investigadores franceses, vinculados al Laboratorio Europeo de Radiación Sincrotrón (ESRF), arribó a esa conclusión comparando las 18 piezas dentarias del fósil con la dentición de otros primates antiguos.</s><s>Mediante el uso de una técnica denominada microtomografía, elaboraron modelos tridimensionales de la estructura de cada diente y de la mandíbula del macho y la hembra de la nueva especie, llamada Lufengpithecus chiangmuanensis , con una resolución de una millonésima de metro.</s>
</p>
<p><s>"Nunca tendremos la seguridad de que se trata de un ancestro directo, pero es algo bastante cercano", dice Jean-Jacques Jaeger, paleontólogo de la Universidad de Montpellier, Francia, uno de los autores de este trabajo, publicado en Nature .</s><s>La hipótesis cobra fuerza debido al hecho de que había polen fosilizado junto a los dientes, lo que sugería que la nueva especie vivió en un bosque tropical, tal como los actuales orangutanes.</s>
</p>
</text>
