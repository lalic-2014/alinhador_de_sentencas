#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Ultima atualizacao: 02/07/2012

# Exemplo de uso
# perl TCAalign.pl -s in/pt_com_tags.txt -t in/es_con_tags.txt -so out/pt-es_pt.txt -to out/pt-es_es.txt -tg

# Programa: TCAalign.pl
# Autor: Helena Caseli (LaLiC)
# Funcao: Alinha as sentencas dos textos paralelos passados como parametro.
# Criterios de alinhamento:
#   Cognatos: LCSR (threshold=0.65) ou Coeficiente de Dice (threshold=0.65)
#   Candidatas a nomes proprios: palavras com inicial maiuscula
#   Caracteres especiais: ?,!,$,@,%
#   Lista de palavras ancoras (opcional): arquivo como parametro
# Entrada: Dois arquivos com cada sentenca em uma linha OU etiquetados com 
# delimitadores de texto (text), paragrafo (p) e sentenca(s) (usar opcao -tg)
# Saida: Dois arquivos com sentencas de entrada alinhadas
#
# OBS.: Ha uma opcao de gerar apenas alinhamentos 1:1 (-o)

# Heuristicas que evitam alinhamentos de omissao implementadas - Buscar "HEURISTICAS"
# Heuristicas de (AZIZ; SPECIA, 2011):
# - rearranja alinhamentos 0-1 e 1-0 em um alinhamento 1-1 => H1 implementada
# - rearranja alinhamentos 1-2 e 2-1 em dois alinhamentos 1-1 => H2 nao implementada
# Minhas heuristicas
# + rearranja alinhamentos 1-0 e 1-2 em um alinhamento 1-1 => H3 implementada
# + rearranja alinhamentos 0-1 e 2-1 em um alinhamento 1-1 => H3 implementada


use FileHandle;
use locale;
use strict;
use Getopt::Long;
use Pod::Usage;
use PorTAl::Aux;

my($arqfonte,$arqalvo,$lpa,$help,$verbose,$dev,$um_para_um,%ancfonte,%ancalvo,$saidafonte,$saidaalvo);

# Parametros
my $lowind = 0.20; 
my $highind = 0.40;
my $toohigh = 0.50;
my $limite = 0.65;	
my $janela = 5;
my $metrica = 'dice';
my $etiquetado = $um_para_um = 0;
$lpa = '';

GetOptions( 'source|s=s' 			=> \$arqfonte,
	 			'target|t=s' 			=> \$arqalvo,
				'sourceoutput|so=s'		=> \$saidafonte,
				'targetoutput|to=s'		=> \$saidaalvo,
				'lpa|l=s' 				=> \$lpa,
				'onetoone|o' 			=> \$um_para_um,
				'metric|m' 				=> \$metrica,
				'threshold|th=f' 	=> \$limite,
				'tagged|tg'				=> \$etiquetado,
				'help|h|?'				=> \$help,
				'verbose|v'				=> \$verbose,
				'development|d'		=> \$dev
			) || pod2usage(2);
	pod2usage(2) if $help;
	pod2usage(2) unless ($arqfonte && $arqalvo && $saidafonte && $saidaalvo);
	if (($metrica ne 'lcsr') && ($metrica ne 'dice')) { 
		print "Choose one of these cognate metrics: lcsr or dice\n"; 
		exit 1;
	}

if ($lpa ne '') { le_LPA($lpa,\%ancfonte,\%ancalvo); }
else { %ancfonte = %ancalvo = (); }

alinha($arqfonte,$arqalvo,\%ancfonte,\%ancalvo,$saidafonte,$saidaalvo);

if ($verbose) { print "\n"; }


sub alinha {
	my($arqfonte,$arqalvo,$htrans,$vtrans,$saidafonte,$saidaalvo) = @_;
	my($saida) = 1;
	my(%sfonte,%salvo,@lenf,@lena,@listaf,@listaa,@matrix,@auxfonte,@auxalvo);
	my($l,$c,$contf,$conta,$qtdf,$qtda,$melhor,@idsf,@idsa,@alf,@ala,$cabf,$caba,$i,@aux);
	my $idali = 1; # indice do alinhamento quando a entrada nao esta etiquetada

	%sfonte = %salvo = @lenf = @lena = @listaf = @listaa = @matrix = @auxfonte = @auxalvo = ();
	
	if ($verbose) { print "\nAligning $arqfonte and $arqalvo ...\n";  }
	
	if ($etiquetado) {
		$cabf = le_sentencas($arqfonte,\%sfonte,0); # Nao passar parametro 1 (separa tokens)
		$caba = le_sentencas($arqalvo,\%salvo,0);
		@idsf = sort {compara($a,$b)} keys %sfonte;
		@idsa = sort {compara($a,$b)} keys %salvo;
	}
	else {
		le_sentencas_cruas($arqfonte,\%sfonte);
		le_sentencas_cruas($arqalvo,\%salvo);
		@idsf = sort {int($a) <=> int($b)} keys %sfonte;
		@idsa = sort {int($a) <=> int($b)} keys %salvo;
	}	

	zera_arquivo($saidafonte);
	zera_arquivo($saidaalvo);

	if ($etiquetado) { 
		imprime_cabecalho($saidafonte,$cabf); 
		imprime_cabecalho($saidaalvo,$caba); 
	}

	if ($verbose) { print "Aligning ",$#idsf+1," source sentences and ",$#idsa+1," target sentences\n" }
	
	$contf = $conta = 0;
	while (($#idsf >= 0) && ($#idsa >= 0)) { 
		if ($um_para_um) { $qtdf = $qtda = 1; }
		else {
			# Busca sentencas a alinhar
			while (($contf < $janela) && ($contf <= $#idsf)) { 
				listas($sfonte{$idsf[$contf]}{'sentenca'},$contf,$htrans,\@listaf);
				push(@lenf,length($sfonte{$idsf[$contf]}{'sentenca'}));
				$contf++;
			}
			while (($conta < $janela) && ($conta <= $#idsa)) { 
				listas($salvo{$idsa[$conta]}{'sentenca'},$conta,$vtrans,\@listaa);
				push(@lena,length($salvo{$idsa[$conta]}{'sentenca'}));
				$conta++;
			}

			if ($#matrix < 0) { 
				$qtdf = $contf; 
				$qtda = $conta; 			
			}

			if ($dev){
				imprime_listas(\@listaf);		
				imprime_listas(\@listaa); 
			}
    	# Prepara a matriz para receber os valores das combinacoes de sentencas
			zera_matrix($contf-$qtdf,$contf-1,$conta-$qtda,$conta-1,\@matrix);
			# Completa a matriz
			completa_matrix($contf-$qtdf,$contf-1,$conta-$qtda,$conta-1,\@listaf,\@listaa,$htrans,\%sfonte,\@idsf,\%salvo,\@idsa,\@matrix);


			if ($dev) { imprime_matrix(\@lenf,\@lena,$conta,$contf,\@matrix); }
		
			$melhor = combinacoes($contf,$conta,\@lenf,\@lena,\@matrix);	

			if ($dev) { print "\n=> BEST: $melhor\n\n"; }
				@aux = split(/ /,$melhor);	

			# HEURISTICAS - INICIO
			# Heuristicas sugeridas por (AZIZ; SPECIA, 2011) - STIL
			# H1: Se @aux contem (1:0,0:1,...) ou (0:1,1:0,...) => Gera 1 alinhamento 1:1
			if (($aux[0] =~ /1:0/) && ($aux[1] =~ /0:1/)) {
				$aux[0] = "1:1";
			}
			# H2: Se @aux contem (1:2,2:1,...) ou (2:1,1:2,...) => Gera 2 alinhamentos 1:1
			# => Nao implementada
			# H3: Nova heuristica que evita ao maximo alinhamentos de omissao
			if (($aux[0] =~ /0:1/) && ($aux[1] =~ /2:1/)) {
				$aux[0] = "1:1";
			}
			elsif (($aux[0] =~ /1:0/) && ($aux[1] =~ /1:2/)) {
				$aux[0] = "1:1";
			}
			# HEURISTICAS - FIM			

			($qtdf,$qtda) = split(/:/,$aux[0]);
			if (($qtdf == 0) && ($qtda == 0)) { $qtdf = $qtda = 1; }
			desloca($qtdf,$qtda,\@lenf,\@lena,\@listaf,\@listaa,\@matrix);
			$contf = $contf-$qtdf;
			$conta = $conta-$qtda;			
		}
		@alf = @ala = ();

		# Para forcar um tipo especifico de alinhamento, alterar AQUI $qtdf e $qtda. Ex: $qtdf = 2 e $qtda = 1 vai gerar um alinhamento 2:1
#		$qtdf = 1;
#		$qtda = 0;

		if ($qtdf > 0) { @alf = splice(@idsf,0,$qtdf); }
		if ($qtda > 0) { @ala = splice(@idsa,0,$qtda); }
		imprime_sentenca_alinhada($saidafonte,\%sfonte,\@alf,\@ala,$etiquetado,$idali,'t'.$qtdf.':'.$qtda); 
		imprime_sentenca_alinhada($saidaalvo,\%salvo,\@ala,\@alf,$etiquetado,$idali++,'t'.$qtdf.':'.$qtda); 
		if ($verbose) { print "Alignment $qtdf:$qtda\n";	}

		if ($dev) {
	 		if ($qtdf != $qtda) { 
	 			$saida = 0; 
	 			print "VERIFY: $qtdf:$qtda = ",join(" ",@alf),"<=>",join(" ",@ala),"\n";
	 			print join("\n",map($sfonte{$_}{'sentenca'},@alf)),"\n",join("\n",map($salvo{$_}{'sentenca'},@ala)),"\n\n";
	 		}
		}
 	}
	while ($#idsf >= 0) { 
		@alf = splice(@idsf,0,1);
		@ala = ();
		imprime_sentenca_alinhada($saidafonte,\%sfonte,\@alf,\@ala,$etiquetado,$idali++,'t1:0');
		if ($verbose) { print "Alignment 1:0\n"; }
	}
	while ($#idsa >= 0) { 
		@ala = splice(@idsa,0,1);
		@alf = ();
		imprime_sentenca_alinhada($saidaalvo,\%salvo,\@ala,\@alf,$etiquetado,$idali++,'t0:1');
		if ($verbose) { print "Alignment 0:1\n"; }
	}
	if ($etiquetado) {
		imprime_fim($saidafonte);
		imprime_fim($saidaalvo);
	}

	return $saida;
}
	
sub desloca {
	my($qtdl,$qtdc,$lenf,$lena,$listaf,$listaa,$matrix) = @_;
	my($i);
	
	for($i = 1;$i <= $qtdl;$i++) { shift(@$lenf); shift(@$listaf); }
	for($i = 1;$i <= $qtdc;$i++) { shift(@$lena); shift(@$listaa); }
	
	while ($qtdl > 0) {
		shift(@$matrix);
		$qtdl--;
	}
	while ($qtdc > 0) {
		for($i=0;$i <= $#$matrix;$i++) { shift(@{$$matrix[$i]}); }
		$qtdc--;
	}
}

###############################################################################################################
#                                                	LPA
###############################################################################################################
# Retorna em $resp todas as ocorrencias de $palavra em $hash: ela mesma, truncada (com *) ou em expressoes.
sub match {
  my($palavra,$hash) = @_;
  my($h,$h1,@tru,@resp);

  $palavra = lc($palavra);
  @resp = grep(/^$palavra$|^$palavra | ($palavra)$/,keys %{$hash});
  @tru = grep($palavra =~ /$_/,grep(/\*/,keys %{$hash})); #todas as palavras truncadas (com *)
  if ($#tru >= 0) { 
    map(s/\*$//,@tru);
    map(pertence($_,\@resp) == 0 ? push(@resp,$_) : (),@tru);
  }
  return @resp;	
};

# Retira de $aux todas as ocorrencias nao presentes em $sentenca e altera a sentenca removendo as palavras
# ja testadas.
sub expressoes {
	my($sentenca,$aux) = @_;
	my($min,$i,$faz,$sub,$palavra);
	
	if ($#{$aux} >= 0) {
		$min = lc($$sentenca);
		$faz = 0;
		$i = 0;
		$palavra = $$aux[$i];
		while ($i <= $#{$aux}) {
			if (index($min,$$aux[$i]) == -1) { 
			  delete($$aux[$i]);
			} else {
				$faz = 1;
				if (length($palavra) > length($$aux[$i])) {
					$palavra = $$aux[$i];
				}
			}
			$i++;
		}
		if ($faz) {
			$i = index($min,$palavra);
			$sub = substr($$sentenca,0,$i);
			$sub .= substr($$sentenca,$i+length($palavra),length($$sentenca)-length($palavra));
		}
		$$sentenca = $sub;
	}
}

# $lpa = arquivo contendo a lista de palavras ancoras no formato:
# palavra_fonte <> palavra_alvo
# Le a LPA e armazena as palavras fonte e alvo em $htrans e $vtrans
sub le_LPA {  
  my($lpa,$htrans,$vtrans) = @_;
  my($hword,$vword);

  if (defined $lpa) {
    if ($verbose) { print "Reading LPA..."; }
    if (open(T,$lpa) != 1) { 
			erro(2,$lpa);
		}
    while (<T>) {
		chop;
		($hword, $vword) = split(/ <> /, $_);
		push(@{$$htrans{$hword}}, $vword);
		push(@{$$vtrans{$vword}}, $hword);		
    };
    close(T);
    if ($verbose) { print " OK!\n"; }
  };
};

###############################################################################################################
#                                                LISTAS DE FEATURES
###############################################################################################################
# Insere em $lista, na posicao $id, as listas de palavras ancoras, candidatas a nome
# proprio e caracteres especiais de $sentenca
sub listas {
  my($sentenca,$id,$hash,$lista) = @_;
  my(@aux,@ancoras,@maiusculas,@caracteres,$i,@tokens,%hash,$x);

  @ancoras = @maiusculas = @caracteres = ();  
  if ($sentenca =~ /<w id=/) { # remove tags
    remove_etiquetas(\$sentenca);
  }
  @tokens = split(/ +/,separa_tokens($sentenca));
	
  @caracteres = grep(caracter($_),@tokens);
	@tokens = grep(palavra($_),@tokens); # so palavras
	@maiusculas = grep(inicial_maiuscula($_),@tokens);
	
	$i = 0;
	while ($i <= $#tokens) {
		@aux = match($tokens[$i],$hash);			
		expressoes(\$sentenca,\@aux);
		$x = 0;
		while ($x <= $#aux) { 
			if (defined $aux[$x]) {	push(@ancoras,$aux[$x]); }
			$x++;		
		}
		$i++;
	}
			
	if ($#ancoras >= 0) { push(@{$$lista[$id]{'a'}},@ancoras); }
	if ($#caracteres >= 0) { push(@{$$lista[$id]{'c'}},@caracteres); }
	if ($#maiusculas >= 0) { @{$$lista[$id]{'m'}} = @maiusculas;	}
};  

###############################################################################################################
#                                                MATRIZ
###############################################################################################################
# Calcula o indice de correspondencia entre duas sentencas a partir do tamanho delas
# - $tamf e $tama.
sub indice {
	my($tamf,$tama) = @_;
	
	return (abs($tamf-$tama))/(($tamf+$tama)/2);
}

# Retorna o $valor alterado em relacao ao indice $ind e os parametros.
sub sum {
	my($valor,$comb,$ind) = @_;
		
	if ($ind <= $lowind) {	$valor++; } 
	elsif ($ind >= $highind) { $valor--; }
	if (($comb == 1) && ($ind >= $toohigh)) { #or (($comb == 2) and ($ind >= $lowind))) {
		$valor = -10;
	}	
	return $valor;
}

# Retorna o numero de elementos em comum nos vetores $auxf e $auxa.
sub compara_vetores {
	my($auxf,$auxa,$resp) = @_;
	my($i,@aux,$elemento);
	
	@aux = map(lc($_),@$auxa);
	@$resp = grep(pertence(lc($_),\@aux),@$auxf);
}

# Retorna em $anc um hash tendo como chaves todas as palavras ancoras encontradas em
# $ancf e como valores suas correspondentes em $anca.
sub ancoras {
	my($ancf,$anca,$anc,$htrans) = @_;
	my($i,$j,$pfonte,$palvo,@aux,@jatestados);
	
	@aux = @jatestados = ();
	for($i = 0;$i <= $#$ancf;$i++) {
		$pfonte = quotemeta($$ancf[$i]);
		$j = 0;
		while ($j <= $#$anca) {
			@aux = grep(/^$j$/,@jatestados);
			if ($#aux < 0) {
				$palvo = quotemeta($$anca[$j]);				
				@aux = grep(/^$palvo$/,@{$$htrans{$pfonte}});
				if ($#aux >= 0) {
					push(@{$$anc{$pfonte}},$palvo);
					push(@jatestados,$j);
					$j = $#$anca+1;
				}
			}
			$j++;
		}
	}
}

sub zera_matrix {
	my($li,$lf,$ci,$cf,$m) = @_;
	my($contl,$contc);
	
	for($contl = $li; $contl <= $lf; $contl++) {
		for($contc = 0; $contc <= $cf; $contc++) { ${$$m[$contl]}[$contc] = 0; }	    
	}
	if ($li != 0) {
		for($contc = $ci; $contc <= $cf; $contc++) {
			for($contl = 0; $contl <= $lf-($lf-$li+1); $contl++) { ${$$m[$contl]}[$contc] = 0; }  
		}
	}
}

sub completa_matrix {
	my($li,$lf,$ci,$cf,$listax,$listay,$ancoras,$sx,$idx,$sy,$idy,$m) = @_;
	my($contl,$contc,%anc,@mai,@char);
	
	for($contl = $li; $contl <= $lf; $contl++) {
		for($contc = 0; $contc <= $cf; $contc++) { 
			%anc = @mai = @char = ();
			ancoras($$listax[$contl]{'a'},$$listay[$contc]{'a'},\%anc,$ancoras);
			${$$m[$contl]}[$contc] += quant(\%anc);			
			compara_vetores(\@{$$listax[$contl]{'c'}},\@{$$listay[$contc]{'c'}},\@char);	
			compara_vetores(\@{$$listax[$contl]{'m'}},\@{$$listay[$contc]{'m'}},\@mai);			
			${$$m[$contl]}[$contc] += $#char+1 + $#mai+1;
			${$$m[$contl]}[$contc] += cognatos($$sx{$$idx[$contl]},$$sy{$$idy[$contc]},\@{$$listax[$contl]{'a'}},\@mai);
		}
	}

	for($contc = $ci; $contc <= $cf; $contc++) {
		for($contl = 0; $contl < $li; $contl++) { 
			%anc = @mai = @char = ();
			ancoras($$listax[$contl]{'a'},$$listay[$contc]{'a'},\%anc,$ancoras);
			${$$m[$contl]}[$contc] += quant(\%anc);
			compara_vetores($$listax[$contl]{'c'},$$listay[$contc]{'c'},\@char);	
			compara_vetores($$listax[$contl]{'m'},$$listay[$contc]{'m'},\@mai);
			${$$m[$contl]}[$contc] += $#char+1 + $#mai+1;
			${$$m[$contl]}[$contc] += cognatos($$sx{$$idx[$contl]},$$sy{$$idy[$contc]},\@{$$listax[$contl]{'a'}},\@mai);
		}
	}

	
}


###############################################################################################################
#                                                COGNATOS
###############################################################################################################
sub lcs {
    my ($str1, $str2) = @_;
    my (@m, $i, $j);
    
    my (@c1) = split('', $str1);
    my (@c2) = split('', $str2);

   if ($c1[0] eq $c2[0]) { $m[0][0] = 1; }
   else { $m[0][0] = 0; };

   for ($i = 1; $i <= $#c1; $i++) {
       if ($c1[$i] eq $c2[0]) {
          do {
            $m[$i][0] = 1;
            $i++;
          } until ($i > $#c1);
       }
       else { $m[$i][0] = 0; };
   };

   for ($j = 1; $j <= $#c2; $j++) {
       if ($c1[0] eq $c2[$j]) {
          do {
            $m[0][$j] = 1;
            $j++;
          } until ($j > $#c2);
       }
       else { $m[0][$j] = 0; };
   };

   for ($i = 1; $i <= $#c1; $i++) {
       for ($j = 1; $j <= $#c2; $j++) {
           if ($c1[$i] eq $c2[$j]) { $m[$i][$j] = $m[$i-1][$j-1] + 1; }
           else { $m[$i][$j] = $m[$i-1][$j] > $m[$i][$j-1] ? $m[$i-1][$j] : $m[$i][$j-1]; };
       };
   };    

   return $m[$#c1][$#c2];
};

sub lcsr {
   my ($fonte, $alvo) = @_;
   my($lcsub,$max); 

   if ($fonte eq $alvo) { return 1; };
   $max = length($fonte) > length($alvo) ? length($fonte) : length($alvo);
   $lcsub = lcs($fonte, $alvo)/$max;
   return $lcsub;
};

# Coloca em $array todos os bigramas de $palavra
sub bigramas {
	my($palavra,$array) = @_;
	my($i);
	
	$i = 0;
	while (($i+1) < length($palavra)) {
		push(@{$array},substr($palavra,$i,2));
		$i++;		
	}		 
}

# Retorna o valor do coeficiente de Dice calculado para as palavras pfonte e palvo
sub Dice {
	my($pfonte,$palvo) = @_;
	my(@bfonte,@balvo,$bi,$a,@aux);
	
	$a = 0;
	bigramas($pfonte,\@bfonte);
	bigramas($palvo,\@balvo);
	if (($#bfonte >= 0) and ($#balvo >= 0)) {
		foreach $bi (@bfonte) {
			@aux = grep(/^$bi$/,@balvo);
			if ($#aux >= 0) {
				$a += $#aux + 1;
			}
		}
		if ($a > 0) {
			return ((2*$a)/($#bfonte+$#balvo+2));						
		}else { return 0; }
	}else { return 0; }
}

sub tem_cognato {
	my($palavra,$array) = @_;

	if ($metrica eq 'lcsr') { return grep(lcsr($palavra,$_) >= $limite,@$array); }
	elsif ($metrica eq 'dice') { return grep(Dice($palavra,$_) >= $limite,@$array); }
	return 0;
}

# Retorna o numero de palavras fonte na sentenca fonte que possuem cognatos no lado alvo. As palavras cognatas nao podem pertencer a $anc ou $mai.
sub cognatos {	
	my($sentf,$senta,$anc,$mai) = @_;
	my(@palavrasf,@palavrasa,@aux);
	
	@palavrasf = split(/ +/,separa_tokens($sentf));
	@palavrasf = grep(palavra($_),@palavrasf); # so palavras
	@aux = map(lc($_),@$anc);
	@palavrasf = grep(pertence(lc($_),\@aux) == 0,@palavrasf);
	@aux = map(lc($_),@$mai);
	@palavrasf = grep(pertence(lc($_),\@aux) == 0,@palavrasf);

	@palavrasa = split(/ +/,separa_tokens($senta));
	@palavrasa = grep(palavra($_),@palavrasa); # so palavras

	return grep(tem_cognato($_,\@palavrasa),@palavrasf);	
}

###############################################################################################################
#                                                COMBINACOES
###############################################################################################################
sub combinacoes {
	my($contf,$conta,$tamf,$tama,$m) = @_;
	my(%lista,%testados,$soma,$comb,$max,$melhor,$l,$c,$s);
	
    	$max = 0;
	$melhor = '';  
	%lista = %testados = ();  
	insere(\%lista,\%testados,1,1,0,''); 	
	
	while ($#{$lista{'i'}} >= 0) {		
		remove(\%lista,\$l,\$c,\$soma,\$comb);
		push(@{$testados{'i'}},$l);
		push(@{$testados{'j'}},$c);
		push(@{$testados{'soma'}},$soma);
		
		if ($soma > $max) {
			$max = $soma;
			$melhor = $comb;
		}
		if ($dev) { print "\n$l $c $soma \ncombinations: $comb\n"; }
	
		if (($l <= $contf) && (($c+1) <= $conta)) {
			$s = indice($$tamf[$l-1],$$tama[$c-1]+$$tama[$c]);
			if ($dev) { print "Indice $l <=> $c,".($c+1).": $s\n"; }
			$s = sum($soma+$$m[$l-1][$c-1]+$$m[$l-1][$c],1,$s);
			insere(\%lista,\%testados,$l+1,$c+2,$s,$comb."1:2 ");
		}
		
		if ((($l+1) <= $contf) && ($c <= $conta)) {			
			$s = indice($$tamf[$l-1]+$$tamf[$l],$$tama[$c-1]);
			if ($dev) { print "Indice $l,".($l+1)." <=> $c: $s\n"; }
			$s = sum($soma+$$m[$l-1][$c-1]+$$m[$l][$c-1],1,$s);
			insere(\%lista,\%testados,$l+2,$c+1,$s,$comb."2:1 ");
		}
		if ($l <= $contf) {						
			insere(\%lista,\%testados,$l+1,$c,$soma,$comb."1:0 ");
		}
		if ($c <= $conta) {			
			insere(\%lista,\%testados,$l,$c+1,$soma,$comb."0:1 ");
		}
		if (($l <= $contf) && ($c <= $conta)) {			
			$s = indice($$tamf[$l-1],$$tama[$c-1]);
			if ($dev) { print "Indice $l <=> $c: $s\n"; }
			$s = sum($soma+$$m[$l-1][$c-1],0,$s);
			insere(\%lista,\%testados,$l+1,$c+1,$s,$comb."1:1 "); 
		}	
	}
	return $melhor;
}

# Insere na lista de combinacoes os valores de $i, $j, $soma e $comb
sub insere {
	my($lista,$testados,$i,$j,$soma,$comb) = @_;
	my($contl,$contt);	
	
	if ($soma < 0) { return 0; }
	$contl = $contt = 0;
	while ($contl <= $#{$$lista{'i'}}) { 
		if (($$lista{'i'}[$contl] == $i) && ($$lista{'j'}[$contl] == $j) && ($$lista{'soma'}[$contl] > $soma)) {
			last;			
		}
		$contl++;
	}	
	while ($contt <= $#{$$testados{'i'}}) { 
		if (($$testados{'i'}[$contt] == $i) && ($$testados{'j'}[$contt] == $j) && 
			 ($$testados{'soma'}[$contt] > $soma)) {
			last;			
		}
		$contt++;
	}	
	if (($contl > $#{$$lista{'i'}}) && ($contt > $#{$$testados{'i'}})) { 
		push(@{$$lista{'i'}},$i);
		push(@{$$lista{'j'}},$j);
		push(@{$$lista{'soma'}},$soma);
		push(@{$$lista{'comb'}},$comb);							
	}
}

# Remove o elemento no topo da lista de combinacoes e retorna os valores em $i, $j
# $soma e $comb.
sub remove {
	my($lista,$i,$j,$soma,$comb) = @_;
	
	$$i = pop(@{$$lista{'i'}});
	$$j = pop(@{$$lista{'j'}});
	$$soma = pop(@{$$lista{'soma'}});
	$$comb = pop(@{$$lista{'comb'}});	
}

###############################################################################################################
#                                                ENTRADA/SAIDA
###############################################################################################################
# Imprime a matriz nXn
sub imprime_matrix {
	my($lenf,$lena,$conta,$contf,$m) = @_;
	my($c,$l);
	
	for($c = 0; $c < $conta; $c++) { print  "\t\t$c"; }
	print "\n";
	for($c = 0; $c < $conta; $c++) { print "\t\t",$$lena[$c]; }
	print "\n";
	for($l = 0; $l < $contf; $l++) {
		print "$l ",$$lenf[$l];
		for($c = 0; $c < $conta; $c++) { print "\t\t$$m[$l][$c]"; }	    
		print "\n";
	}	
}

# Imprime as listas de palavras ancoras, maiusculas e caracteres especiais contidas em $lista
sub imprime_listas {
	my($lista) = @_;
	my($i);
	
	for($i = 0; $i <= $#$lista;$i++) {
		print "$i";
		if ($#{$$lista[$i]{'a'}} >= 0) { print "\nAnchors: ",join(",",@{$$lista[$i]{'a'}}); }
		if ($#{$$lista[$i]{'m'}} >= 0) { print "\nUpper Case:",join(",",@{$$lista[$i]{'m'}}); }
		if ($#{$$lista[$i]{'c'}} >= 0) { print "\nCaracters:",join(",",@{$$lista[$i]{'c'}}); }
		print "\n";
	}	
}

###############################################################################################################
#                                                AUXILIARES
###############################################################################################################
sub caracter {
  my($c) = @_;
  if (($c eq '!') || ($c eq '?') || ($c eq '%') || ($c eq '$') || ($c eq '@')) { return 1; }
  return 0;
}  

sub palavra {
  my($str) = @_;
  if ($str =~ /[\wÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃẼĨÕŨÄËÏÖÜÇÑáéíóúàèìòùâêîôûãẽĩõũäëïöüçñ]/) { return 1;}  
  return 0;
}

sub inicial_maiuscula {
  my($str) = @_;
  return ($str =~ /^[A-ZÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃẼĨÕŨÄËÏÖÜ]/);
}

#retorna o nome do arquivo sem extensoes nem diretorios
sub nome {
  my($arq) = $_[0];
  my(@aux,$c);
  @aux = split(/\//,$arq);
  $arq = $aux[$#aux];
  if ($arq =~ /\./) { do { $c = chop($arq); } until ($c eq '.'); }   
  return $arq;
}

# Retorna o numero de elementos em todos os arrays dos values de $hash.
sub quant {
	my($hash) = @_;
	my($i,@array,$qtd);
	
	$qtd = 0;
	while (($i,@array) = each(%$hash)) { $qtd += ($#array+1); }
	return $qtd;
}

sub pertence {
	my($elemento,$array) = @_;
	my(%aux) = map(($_,0),@$array);
	return defined($aux{$elemento});
}

__END__

=head1 NAME

TCAalign - Sentence aligner based on Translation Corpus Aligner (Hofland, 1996)

=head1 SYNOPSIS

TCAalign [options...]

 Options:
-source|s         input source text file (required)
-target|t         input target text file (required)
-sourceoutput|so  output source text file (required)
-targetoutput|to  output target text file (required)
-lpa|l            anchor word list (optional)
-onetoone|o       if you want to force 1:1 alignments (def=no)
-metric|m         cognate metric can be lcsr or dice (def=dice)
-threshold|th     threshold to cognate metric (def=0.65)
-tagged|tg        input texts are tagged with sentence boundaries (def=no)
-development|d    development option, prints extra information (optional)
-verbose|v        verbose (optional)
-help|h|?         this guide

Usage Example:

  perl TCAalign.pl -s in/pt_com_tags.txt -t in/es_con_tags.txt -so out/pt-es_pt.txt -to out/pt-es_es.txt -tg

Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)

=cut
